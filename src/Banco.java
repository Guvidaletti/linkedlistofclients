import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Banco implements Runnable{
    private String nome,statusArquivoTexto="";
    private LinkedListOfClientes fila;
    private ArrayList<CaixaEletronico> caixas;
    public ArrayList<String> relatorio;
    private FilaTela ft;
    private int numeroDeCaixas,idNormal=0,idPrioritario=0,contador,tamanhoMaximoFila=0;
    public int maiorTempoNaFila=0,clientesAtendidos=0;


    /**
     * Método Construtor da classe Banco
     * @param nome Nome do Banco
     * @param numeroDeCaixas Número de CaixasEletronicos desejados
     */
    public Banco(String nome,int numeroDeCaixas) {
        this.nome = nome;
        this.fila = new LinkedListOfClientes();
        caixas = new ArrayList<>();
        relatorio = new ArrayList<>();
        this.numeroDeCaixas = numeroDeCaixas;
        this.ft = new FilaTela(this);
        for(int i=0;i<this.numeroDeCaixas/2;i++){
            adicionarCaixaPrioritario();
        }
        if(numeroDeCaixas%2!=0)adicionarCaixaNormal();
        for(int i=0;i<this.numeroDeCaixas/2;i++){
            adicionarCaixaNormal();
        }
    }

    /**
     * Método para adicionar um caixa eletronico normal ao Banco
     */
    public void adicionarCaixaNormal(){
        String id = "N";
        idNormal++;
        if (idNormal <10)id=id+"0";
        id=id+ idNormal;
        //System.out.println("add normal, id: " + id);
        contador++;
        CaixaEletronico caixa = new CaixaEletronico(id,1,this,contador);
        caixas.add(caixa);

    }

    /**
     * Método para adicionar um caixa eletronico prioritário ao banco
     */
    public void adicionarCaixaPrioritario(){
        String id = "P";
        idPrioritario++;
        if (idPrioritario <10)id=id+"0";
        id=id+ idPrioritario;
        //System.out.println("Add prioritario, id: " + id);
        contador++;
        CaixaEletronico caixa = new CaixaEletronico(id,2,this,contador);
        caixas.add(caixa);
    }

    /**
     * Retorna a lista de caixar eletronicos do banco
     * @return Lista de caixas eletronicos
     */
    public ArrayList<CaixaEletronico> ListarCaixas(){return this.caixas;}

    /**
     * Adicionar Cliente na fila
     * @param c
     */
    public void entraNaFila(Cliente c) {
        //System.out.println(c + " entrou na fila");
        fila.adicionar(c);
        if(fila.tamanho() > tamanhoMaximoFila)tamanhoMaximoFila = fila.tamanho();

        ft.atulizar(fila.ListarTodos());
    }

    /**
     * utilizado para chamar o método RUN
     * e cria uma Thread para o próximo caixa eletronico atender à fila
     */
    public void atender() {
        Thread t = new Thread(this);
        if(!fila.vazia())t.start();
    }

    /**
     * método para carregar os clientes predefinidos em arquivo texto
     * @throws IOException
     */
    public void carregaClientes() throws IOException {
        InsereClientes ic = new InsereClientes(this);
    }

    /**
     * Seta o Status do Arquivo Texto. se pôde ser carregado ou não
     * @param stat
     */
    public void setStatusArquivoTexto(String stat){
        this.statusArquivoTexto = stat;
    }

    /**
     * recebe o Status do Arquivo Texto. se pôde ser carregado ou não
     * @return
     */
    public String getStatusArquivoTexto(){return statusArquivoTexto;}

    /**
     * Método para retornar a fila de Clientes
     * @return Fila de clientes
     */
    public LinkedListOfClientes getFila(){
        return this.fila;
    }

    /**
     * Retornar o nome do banco
     * @return nome do banco
     */
    public String getNome(){return this.nome;}

    /**
     * Metodo para chamar o método que esconde a fila
     * @see FilaTela
     */
    public void escondeFilaTela(){ft.esconderFila();}

    /**
     * método para chamar o método que mostra a fila
     * @see FilaTela
     */

    public void mostrarFilaTela(){ft.mostrarFila();}

    /**
     * Método para gerar o relatório do projeto.
     * Cria um arquivo texto com os dados
     * @throws IOException
     */
    public void geraRelatorio() throws IOException
    //String machine_name = InetAddress.getLocalHost().getHostName()
    {
        Path path1 = Paths.get("relatorio.txt");
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(path1, Charset.forName("utf8")))) {
            //writer.println("Tamanho " + usuarios.size());
            writer.println("Relatorio:");
            writer.println("Banco: " + nome.toUpperCase());
            writer.println("Número de caixas: " + numeroDeCaixas);
            writer.print("Maior tamanho da fila: " + tamanhoMaximoFila);
            if(tamanhoMaximoFila==1)writer.println(" Cliente");
            else writer.println(" Clientes");
            writer.print("Maior tempo na fila: " + maiorTempoNaFila);
            if(maiorTempoNaFila==1) writer.println(" Segundo");
            else writer.println(" Segundos");
            writer.print("Clientes atendidos: " + clientesAtendidos);
            if(clientesAtendidos==1) writer.println(" Cliente");
            else writer.println(" Clientes");
            double tempoMedio = (double) maiorTempoNaFila / clientesAtendidos;
            writer.print("Tempo médio de espera por cliente: " + String.format("%.2f",tempoMedio));
            if(tempoMedio==1)writer.println(" Segundo");
            else writer.println(" Segundos");
            //String.format( "%.2f", dub )

            for (String s: relatorio){
                writer.println(s);
            }
        }
    }
    /**
     * Método que atende à Fila do banco, com o próximo cixa eletronico disponível
     */
    @Override
    public void run() {
        for(CaixaEletronico ca: caixas){
            if(ca.StatusToString().equalsIgnoreCase("LIVRE")){
                String modo = ca.ModoToString();
                if(modo.equalsIgnoreCase("PRIORITARIO")){
                ca.Atendimento(fila.proximoPrioritario());
                if(!fila.vazia())ft.atulizar(fila.ListarTodos());
                else ft.esvaziar();
                break;
                } else if(modo.equalsIgnoreCase("NORMAL")){
                    ca.Atendimento(fila.proximoCliente());
                    if(!fila.vazia())ft.atulizar(fila.ListarTodos());
                    else ft.esvaziar();
                    break;
                }
            }
        }
    }

}