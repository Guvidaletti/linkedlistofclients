public class Cliente {
    private String nome;
    private int idade;
    private long entrouNaFila;
    private long saiuDaFila;

    /**
     * Método Construtor
     *
     * @param nome  - Nome do Cliente
     * @param idade - Idade do Cliente
     */
    public Cliente(String nome, int idade) {
        this.nome = nome;
        this.idade = idade;
    }

    /**
     * Método para utilizar o Nome do Cliente
     *
     * @return - Nome do cliente
     */
    public String getNome() {
        return nome;
    }

    /**
     * Método para utilizar a idade do Cliente
     *
     * @return - Idade do Cliente
     */
    public int getIdade() {
        return idade;
    }

    /**
     * toString
     *
     * @return - "Nome ; Idade"
     */
    public String toString() {
        return nome + ", " + idade;
    }

    /**
     * Método para definir o tempo de entrada do cliente na fila
     * (Milissegundos)
     */
    public void entrouNaFila() {
        this.entrouNaFila = System.currentTimeMillis();
    }

    /**
     * Método para definir o tempo de saída do cliente na fila
     * (Milissegundos)
     */
    public void saiuDaFila(){this.saiuDaFila = System.currentTimeMillis();}

    /**
     * Cálculo do tempo total que o cliente ficou na fila
     * @return Tempo na fila (segundos)
     */
    public int tempoNaFila(){
        int tempo = (int) Math.round((saiuDaFila - entrouNaFila) / 1000.0);
        return tempo;
    }

}