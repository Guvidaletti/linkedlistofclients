import java.util.ArrayList;

public class LinkedListOfClientes{
    private class Node {
        public Cliente cliente;
        public Node next;

        public Node(Cliente cliente)
        {
            this.cliente = cliente;
            next = null;
        }
    }
    private Node head;
    private Node tail;
    private int count;

    /**
     * Método Construtor da Lista
     */
    public LinkedListOfClientes(){
        head = null;
        tail = null;
        count = 0;
    }

    /**
     *  Método para verificar se a lista está vazia
     * @return boolean true - se a lista estiver vazia
     */
    public boolean vazia()
    {
        if(count==0)return true;
        return false;
    }

    /**
     * Método para verificar o tamanho da lista
     * @return int - tamanho da lista
     */
    public int tamanho(){return count;}

    /**
     * Método para limpar a lista
     */
    public void limpa() {
        head=null;
        tail=null;
        count=0;
    }

    /**
     * Método para adicionar um cliente à lista
     * @param cliente - Cliente à ser adicionado
     */
    public void adicionar(Cliente cliente){
        Node aux = new Node(cliente);
        if(vazia())head = aux;
        else tail.next = aux;
        cliente.entrouNaFila();
        tail = aux;
        count++;
    }

    /**
     * Método que chama o próximo cliente à ser chamado
     * por ordem de chegada
     * @return Cliente mais 'antigo' da fila
     */
    public Cliente proximoCliente()
    {
        if(vazia()) return null;
        Node aux = head;
        if(head.next==null)
        {
            head=null;
        }
        else {
            head = head.next;
        }
        count--;
        aux.cliente.saiuDaFila();
        return aux.cliente;
    }

    /**
     * Método que verifica se a fila tem algum cliente
     * com 65 anos ou mais
     * @return true se existe um cliente prioritário, false se não existe
     */
    public boolean temPrioritario(){
        if(vazia())return false;
        Node aux = head;
        for(int i=0;i<count;i++)
        {
            if(aux.cliente.getIdade()>=65)return true;
            aux = aux.next;
        }
        return false;
    }

    /**
     * Método que retorna o próximo cliente prioritário
     * se não existe nenhum prioritário na lista, chama o método
     * proximoCliente()
     * @return Cliente
     */
    public Cliente proximoPrioritario()
    {
        if(vazia())return null;
        if(!temPrioritario())return proximoCliente();
        else
        {
            Node aux = head;
            if (aux.cliente.getIdade()>=65)return proximoCliente();//testa o head
            while (aux.next!=null)
            {
                if(aux.next.cliente.getIdade()>=65){//se o aux.next é prioritario
                    Node prioritario = aux.next; //cria um nodo pra retornar
                    if(tail==aux.next){//testa se o aux.next era o ultimo
                        aux.next = null;
                        tail = aux;
                    }
                    else{ //se aux.next nao era o ultimo,
                        aux.next = aux.next.next;
                    }
                    count--;
                    prioritario.cliente.saiuDaFila();
                    return prioritario.cliente;
                }
                aux=aux.next;
            }
            return proximoCliente();
        }
    }

    /**
     * Lista todos os clientes pertencentes à fila
     * @return ArrayList<Cliente>
     */
    public ArrayList<Cliente> ListarTodos(){
        ArrayList<Cliente> fila = new ArrayList<>();
        if(vazia())return null;
        Node aux = head;
        for(int i=0;i<count;i++)
        {
            fila.add(aux.cliente);
            aux=aux.next;
        }
        return fila;

    }
}