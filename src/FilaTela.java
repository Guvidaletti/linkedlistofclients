import java.util.ArrayList;

public class FilaTela {

    private Banco banco;
    private int count;
    private int total;
    private ArrayList<ClienteTela> fila;

    /**
     * Método Construtor da Fila de Clientes que serão visualizados na tela
     * @see ClienteTela
     * @see Banco
     * @param banco
     */
    public FilaTela(Banco banco){
        this.banco=banco;
        count=0;
        fila = new ArrayList<>();
        construir(7,8);
    }

    /**
     * Método construtor da Fila que é exibida
     * @see ClienteTela
     * @param linhas - numero de linhas da Fila
     * @param colunas - número de colunas da Fila
     */
    public void construir(int linhas, int colunas){
        this.total=linhas*colunas;
        for(int i=0;i<linhas;i++){//7
            if(i%2==0){
                for(int j=0;j<colunas;j++){//8
                    count++;
                    ClienteTela tela = new ClienteTela(count,i,j);
                    fila.add(tela);
                }
            }else {
                    for(int j=colunas;j>=0;j--){//8
                        count++;
                        ClienteTela tela = new ClienteTela(count,i,j);
                        fila.add(tela);
                    }
                }


        }
    }

    /**
     * Atualizar o estado da Fila que está sendo exibida
     * @param list - para verificar o novo estado
     */
    public void atulizar(ArrayList<Cliente> list){
        int i=0;
        esvaziar();
        for(Cliente c: list){
            try{
                fila.get(i).setCliente(c);
            }catch (Exception e){}
            //fila.get(i).setCliente(c);
            i++;
        }
    }

    /**
     * Método para esvaziar a fila na tela,
     * utilizado para atualizar os valores na tela
     */
    public void esvaziar(){
        for(ClienteTela ct: fila){
            if(ct.isLimpo())break;
            ct.limpar();
        }
    }

    /**
     * método para atender ao botão
     * "Esconder/Mostrar Fila"
     */
    public void esconderFila(){
        for(ClienteTela ct: fila){
            ct.esconder();
        }
    }

    /**
     * método para atender ao botão
     * "Esconder/Mostrar Fila"
     */
    public void mostrarFila(){
        for(ClienteTela ct: fila){
            ct.mostrar();
        }
    }

}
