import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class InsereClientes implements Runnable {
    private Banco banco;
    private ArrayList<Cliente> lista;

    /**
     * Método para inserir cada cliente da lista randomicamente
     * @see Banco
     * @param banco - Recebe o banco em execução para fazer operações nele
     */
    public InsereClientes(Banco banco){//construtor
        this.banco = banco;
        lista = new ArrayList<>();
        try{
            banco.setStatusArquivoTexto("Arquivo Texto Carregado!");
            carregaClientes();
        }catch(IOException e)
                {
                //System.out.println("problema no InsereClientes");
                banco.setStatusArquivoTexto("Arquivo Texto Não Encontrado!");
                }
        Thread t = new Thread(this);
        t.start();
    }

    /**
     * método para ler o arquivo "ListaClientes.txt"
     * E adicionar clientes de acordo com a lista
     * @throws IOException
     */
    public void carregaClientes() throws IOException {
        Path path2 = Paths.get("ListaClientes.txt");
        try(Scanner sc = new Scanner(Files.newBufferedReader(path2,Charset.forName("utf8")))){
            sc.useDelimiter("[;\n]");
            String header=sc.nextLine();//pulacabeçalho
            while(sc.hasNext()) {
                String nome = sc.next();
                String idd = sc.next();
                try{String espaco = sc.next();}catch (Exception e) {}
                try {
                    int idade = Integer.parseInt(idd);
                    Cliente c = new Cliente(nome, idade);
                    lista.add(c);
                    banco.atender();
                } catch (NumberFormatException e) {System.out.println("Erro ao converter o número");
                }
            }
        }
    }

    /**
     * Método para AUTOMATIZAR a adição de Clientes
     * na fila do banco
     */
    @Override
    public void run() {
        do {
            int rand = (int) (Math.round(Math.random() * 9000) + 1000); //entra de 1 à 10 segundos
            // segundos
            try {
                Thread.sleep(rand);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean taVazia=false;
            if(banco.getFila().vazia()) taVazia=true;
            banco.entraNaFila(lista.get(0));
            if(taVazia)banco.atender();
            lista.remove(0);
        }while (!lista.isEmpty());
    }
}