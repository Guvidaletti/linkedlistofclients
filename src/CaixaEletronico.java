import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class CaixaEletronico implements Runnable{
    public enum Modo {PRIORITARIO , NORMAL};
    public Modo modo;
    public enum Status {OCUPADO,LIVRE};
    public Status status;
    private String id;
    private int Rand;
    private Banco banco;
    private int identificador;
    private CaixaEletronicoTela tela;

    /**
     * Método construtor do caixa
     * @param id - id do caixa eletronico
     * @param modo - modo (1) - NORMAL  (2) - PRIORITARIO
     * @param banco - Receb o banco para fazer operações com o banco sendo utiliado
     * @param identificador - Identificador para posicionar na tela
     */
        public CaixaEletronico (String id, int modo,Banco banco, int identificador)
    {
        this.id=id;
        if(modo==1)this.modo = Modo.NORMAL;
        else if(modo==2)this.modo = Modo.PRIORITARIO;
        this.status=Status.LIVRE;
        this.banco = banco;
        this.identificador = identificador;
        this.tela = new CaixaEletronicoTela(this,banco,identificador);
        tela.setVisible(true);
        tela.atualizar(null,Rand);

    }

    /**
     * Método que chama a inicalização da Thread, coloca o Caixa eletronico como "OCUPADO"
     * Recebe o cliente, cria o tempo AUTOMATIZADO para o atendimento
     * Tempo: de 4 à 20 Segundos de atendimento
     * @param fulano - Cliente à ser atendido
     */
    public void Atendimento(Cliente fulano){
        status = Status.OCUPADO;
        //System.out.println("Caixa "+ modo +" id: " + this.id +
        //" Atendendo ao Cliente: " + fulano.getNome());
        this.Rand =(int) (Math.round(Math.random()*16000)+4000); //atendimento de 4 à 20 segundos
        tela.atualizar(fulano,this.Rand);
        //------------------------------------------PRINT PARA USO DO RELATÓRIO---------------------------------------//
        String omni = "CLIENTE: "+ fulano.getNome() +": " + fulano.tempoNaFila() + " Segundos na fila, " +
                (int) Math.round(this.Rand/1000) + " Segundos no atendimento";
        banco.relatorio.add(omni);
        banco.clientesAtendidos++;
        if(fulano.tempoNaFila() > banco.maiorTempoNaFila) banco.maiorTempoNaFila = fulano.tempoNaFila();
        try{banco.geraRelatorio();}catch(IOException e){
            System.out.println("Problema com o arquivo");
        }
        //Chama o método RUN
        Thread t = new Thread(this);
        t.start();
    }

    /**
     * Método para receber o status atual do Caixa Eletronico
     * @return Status atual do Caixa Eletronico
     */
    public String StatusToString(){
            return "" + status;
    }

    /**
     * Método para receber o Modo do Caixa Eletronico
     * @return String do modo do caixa eletronico
     */
    public String ModoToString(){return "" + modo;}

    /**
     * Método para retornar o id do caixa eletronico
     * @return ID: "N" para normal, "P" para Prioritário, + o número identificador
     */
    public String getId(){return this.id;}

    /**
     * Retorna o necessário para identificar o caixa eletronico
     * @return Caixa Eletronico "TIPO DO CAIXA" e o "ID"
     */
    public String toString(){
            return "Caixa Eletronico " + modo + ": " + id;
    }

    /**
     * Começa o atendimento do caixa eletônico
     * faz ele esperar para simular o atendimento
     * depois seta o status como Livre.
     */
    @Override
    public void run() {
        try {
            Thread.sleep(Rand);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //PRINT DE TESTE
        //System.out.println("Atendimento em: " + (int)(Rand/1000) + " Segundos");
        this.status = Status.LIVRE;
        //System.out.println("Caixa "+ modo +" id: " + this.id + " Está " + status);
        tela.Livre();

    }
}
