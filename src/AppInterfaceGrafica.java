import java.util.ArrayList;
import java.util.Scanner;

public class AppInterfaceGrafica {
    private Banco banco;
    private AppNovoBanco NB;
    private ArrayList<Cliente> lista;

    /**
     * Void Main
     * @param args
     */
    public static void main(String[] args) {
        AppNovoBanco NB = new AppNovoBanco();
        NB.setVisible(true);
    }

    /**
     * Cria um banco novo
     * método utiliado pelos apps de interface gráfica
     * @param banco
     */
    public void newBanco(Banco banco){
        this.banco = banco;
        BancoApp telaBanco = new BancoApp(this.banco);
        telaBanco.setVisible(true);

    }
}
